<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReportOption extends Model
{
    //
    public function checkListOption(){
    	return $this->belongsTo('App\CheckListOption');
    }
}
