<?php
namespace App\Http\Controllers;

use App\CheckListOption;
use App\Http\Controllers\Controller;
use App\Question;
use App\Report;
use App\ReportAnswer;
use App\ReportOption;
use Auth;
use Illuminate\Http\Request;

/**
 *
 */
class CheckList extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('srat.index', ['questions' => Question::all()]);
    }
    public function generate(Request $request)
    {
        $ans = $request->ans;

        if (count($ans) <= 0) {
            return redirect('/check-srat');
        }

        $results = [1 => 0, 2 => 0, 3 => 0, 4 => 0, 5 => 0, 6 => 0, 7 => 0, 8 => 0];

        $report        = new Report;
        $report->point = 0;
        $report->user_id = Auth::user()->id;
        $report->save();

        foreach ($ans as $key => $value) {

            $value = reset($value);

            $ra              = new ReportAnswer;
            $ra->question_id = $key;
            $ra->answer      = $value;
            $ra->save();

            $question = Question::find($key);

            if ($question->answer == $value) {
                $options = json_decode($question->passed_options);
                foreach ($options as $option) {

                    $clo = CheckListOption::find($option);

                    $results[$clo->id] += $clo->weight;
                }
            }
        }

        foreach ($results as $key => $value) {
            $reportOption                       = new ReportOption;
            $reportOption->check_list_option_id = $key;
            $reportOption->report_id            = $report->id;
            $reportOption->points               = $value;
            //$reportOption->user_id				=	Auth::user()->id;
            $reportOption->save();
        }

        $total         = array_sum($results);
        $report->point = $total;
        $report->save();

        return redirect('/report/' . $report->id);

    }
    public function report($id)
    {
        $report = Report::find($id);
        $exp    = 54.5;

        if ($report->user_id = Auth::user()->id) {

            $chartData = [];

            $checkListOptions = [];
            $reportOptions    = ReportOption::where('report_id', $id)->get();
            foreach ($reportOptions as $reportOption) {
                $checkListOption    = $reportOption->checkListOption;
                $chartData[]        = round($reportOption->points * 100 / $exp, 2);
                $checkListOptions[] = $checkListOption->name;
            }

            $expected = [4.5, 3.0, 6.0, 14.0, 14.0, 1.0, 10.0, 2.0];
            $expected = array_map(function ($var) use ($exp) {return round($var * 100 / $exp, 2);}, $expected);

            return view('srat.report', ['yaxis' => json_encode($chartData), 'xaxis' => json_encode($checkListOptions), 'expected' => json_encode($expected), 'pointsObtained' => $report->point . "/" . $exp]);
        } else {
            return redirect('/');
        }
    }

}
