var app = {};
app.report = function(title,xaxis,yaxis,expected) {
    // Build the chart
    var xaxis = JSON.parse(xaxis);
    var yaxis = JSON.parse(yaxis);

    /*$('#pie-chart').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: title
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                },
                showInLegend: true
            }
        },
        series: [{
            name: 'Security',
            colorByPoint: true,
            data: data
        }]
    });*/
$('#pie-chart').highcharts({
        chart: {
            type: 'column',
            options3d: {
                enabled: true,
                alpha: 10,
                beta: 25,
                depth: 70
            }
        },
        title: {
            text: 'Assessment report generated on behalf of your requirements'
        },
        subtitle:{
        	text:'Report data is %age of degree of security'
        },
        plotOptions: {
            column: {
                depth: 25
            }
        },
        xAxis: {
            categories: xaxis
        },
        yAxis: {
            title: {
                text: null
            }
        },
        series: [{
            name: 'Your Security',
            data: yaxis
        },
        {name:'Expected Security',
        data:expected,
        color:'#dd4545'
    }]
    });
}