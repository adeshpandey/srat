<html>
    <head>
        <title>SRAT - @yield('title')</title>
        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">    
    </head>
    <body>       
        <header class="navbar navbar-static-top bs-docs-nav" id="top" role="banner">
  <div class="container">
    <div class="navbar-header">
      <button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target="#bs-navbar" aria-controls="bs-navbar" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a href="../" class="navbar-brand">SRAT</a>
    </div>
    <nav id="bs-navbar" class="collapse navbar-collapse">      
      <ul class="nav navbar-nav navbar-right">
        <li><a href="/check-srat">CHECK YOUR REQUIREMENT</a></li>        
      </ul>
    </nav>
  </div>
</header>
        <div class="container">
            @yield('content')
        </div>
        <script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
{{-- <script type="text/javascript" src="{{ asset('javascripts/app.js') }}"></script> --}}
    </body>
</html>