@extends('layouts.app')
@section('title','Security Analysis')
@section('content')
<div class="container">
	<div class="row">
		<form action="{{url('/generate')}}" method="post">
		{{ csrf_field() }}
		<div id="questions">
			@foreach($questions as $question)
			<div class="panel panel-default">
				<div class="panel-heading">
					{{$question->question}}
				</div>
				<div class="panel-body">
					<div class="checkbox">
				        <label>
				          <input type="radio" name="ans[{{$question->id}}][]" value="y"> Yes
				        </label>
				      </div>
			      	<div class="checkbox">
			        	<label>
			          		<input type="radio" name="ans[{{$question->id}}][]" value="n"> No
			        	</label>
			      	</div>
				</div>
			</div>
			@endforeach
		</div>
		<div>
			<button type="submit" class="btn btn-success">Generate Report</button>
		</div>
		</form>
	</div>
</div>
@stop
@section('js')
	var asd = 'Hi';
@stop