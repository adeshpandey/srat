@extends('layouts.app')
@section('content')
	<div class="container">
		<div class="row">
			<div id="pie-chart">
				
			</div>
		</div>
		<div class="row">
			<h3>Your requirements has passed only <strong>{{$pointsObtained}}</strong> assessments</h3>
		</div>
	</div>
@stop
@section('js')
app.report('SRAT Assessment','{!! $xaxis !!}', '{!! $yaxis !!}',{!! $expected !!});
@stop